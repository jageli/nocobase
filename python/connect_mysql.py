import mysql.connector

class MySQLConnector:
    def __init__(self, host, user, password, database):
        self.host = host
        self.user = user
        self.password = password
        self.database = database
        self.connection = None

    def connect(self):
        self.connection = mysql.connector.connect(
            host=self.host,
            user=self.user,
            password=self.password,
            database=self.database
        )

    def disconnect(self):
        if self.connection:
            self.connection.close()

    def execute_query(self, query):
        cursor = self.connection.cursor()
        cursor.execute(query)
        result = cursor.fetchall()
        cursor.close()
        return result

    def create_table(self, table_name, columns):
        query = f"CREATE TABLE {table_name} ({columns})"
        self.execute_query(query)

    def insert_data(self, table_name, data):
        columns = ', '.join(data.keys())
        values = ', '.join([f"'{value}'" for value in data.values()])
        query = f"INSERT INTO {table_name} ({columns}) VALUES ({values})"
        self.execute_query(query)
        self.connection.commit()

    def delete_data(self, table_name, condition):
        query = f"DELETE FROM {table_name} WHERE {condition}"
        self.execute_query(query)
        self.connection.commit()

ny=MySQLConnector('10.70.224.18','nocobase','nocobase','nocobase')
ny.connect()
table="TP"
# ny.execute_query(f"ALTER TABLE {table} ADD COLUMN btsid VARCHAR(255)")
# ny.execute_query(f"ALTER TABLE {table} ADD COLUMN cellid VARCHAR(255)")
result = ny.execute_query(f"SELECT btsid,cellid FROM {table}")
print(result)
# for i in range(100):
#     ny.insert_data(table, {'btsid': f'new_btsid_value{i}', 'cellid': f'new_cellid_value{i}'})
# result = ny.execute_query(f"SELECT btsid, cellid FROM {table}")
# print(result)


ny.disconnect()
